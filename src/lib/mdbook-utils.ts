// Markdown Book Publication Utils

// Node Package Imports
import { execSync } from 'child_process';
import { readFileSync, writeFileSync, existsSync } from 'fs';
import { join, sep } from 'path';

import { copySync } from 'cpx';

import { getMdHeaderStr, createTemplate, getSummarySections, 
    SummarySection, getMdContentLinkName, getMdContentLinkRef } from '@pub-tools/mdlib-ts';
// Open Source van eigen maak (n.t.b. onderbrengen waar?)
import { preProcessKrokiMdContent, writePreProcessedDestFile } from '@dgwnu/md-pre-kroki';

// Local Library Modules
import { MdBookConfig } from './mdbook-config';

// Constant Values
const SUMMARY_MD = 'SUMMARY.md';
const GLOSSARY_MD = 'GLOSSARY.md';
const ROOT_LINKS = ['README', 'index'];
const PANDOC_CONFIG = 'pandoc';
const PANDOC_PDF_TEMPLATE = join(PANDOC_CONFIG, 'pandoc-pdf.template');

/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param config Markdown Book Configuration values  
 */
export function parseMd2Book(config: MdBookConfig) {
    console.log('='.repeat(40));
    console.log('MdBookConfig values (TypeScript)');
    console.log('-'.repeat(40));
    console.log(`templatePath = ${config.templatePath}`);
    console.log(`templatePublicPath = ${config.templatePublicPath}`);
    console.log(`templateConfigJsFilePath = ${config.templateConfigJsFilePath}`);
    console.log(`vuePressDependenciesJsonFilePath = ${config.vuePressDependenciesJsonFilePath}`);
    console.log(`mdContentPath = ${config.mdContentPath}`);
    console.log(`docsPath = ${config.docsPath}`);
    console.log(`docsPublicPath = ${config.docsPublicPath}`);
    console.log(`docsConfigJsFilePath = ${config.docsConfigJsFilePath}`);
    console.log(`publicationBase = ${config.publicationBase}`);
    console.log(`publicationDest = ${config.publicationDest}`);
    console.log(`publicationLogo = ${config.publicationLogo}`);
    console.log(`publicationPdfExport = ${config.publicationPdfExport}`);
    console.log(`publicationEpubExport = ${config.publicationEpubExport}`);
    console.log(`publicationGitRepo = ${config.publicationGitRepo}`);
    console.log('='.repeat(40));

    // check existance required SUMMARY
    const summaryFilePath = join(config.mdContentPath, SUMMARY_MD);
    if (!existsSync(summaryFilePath)) {
        throw new Error(`MdBook CLI - Missing required "${SUMMARY_MD}" file!`);
    }

    // Initialize vuePress docs output with template and defaults
    copySync(`${config.templatePublicPath}${sep}**`, config.docsPublicPath);
    copySync(`${config.templateStylesPath}${sep}**`, config.docsStylesPath);
    copySync(join(config.templatePath, PANDOC_PDF_TEMPLATE), join(config.docsPath, PANDOC_PDF_TEMPLATE));

    // Read SUMMARY to process content
    const summaryMdStr = readFileSync(summaryFilePath, 'utf-8');
    const summarySections = getSummarySections(summaryMdStr);

    // Parse VuePress-configuration values into template
    const description = getMdHeaderStr(summaryMdStr);
    const sidebar = preProcessParseSidebar(summarySections, config);
    const configTemplateJs = readFileSync(config.templateConfigJsFilePath, 'utf-8');
    writeFileSync(
        config.docsConfigJsFilePath,
        configTemplateJs
            .replace(createTemplate('base'), config.publicationBase)
            .replace(createTemplate('dest'), config.publicationDest)
            .replace(createTemplate('description'), description)
            .replace(createTemplate('logo'), config.publicationLogo)
            .replace(createTemplate('sidebar'), sidebar)
            .replace(createTemplate('pdfExport'), config.publicationPdfExport)
            .replace(createTemplate('epubExport'), config.publicationEpubExport)
            .replace(createTemplate('gitRepo'), config.publicationGitRepo)
    );

    // copy existing glossary definitions
    const glossaryFilePath = join(config.mdContentPath, GLOSSARY_MD);

    if (existsSync(glossaryFilePath)) {
        copySync(glossaryFilePath, join(config.docsPath, GLOSSARY_MD));
    }

    // export consolidated site markdown-file from pre-processed content
    exportSiteMd(summarySections, config);

}

/**
 * Use summary sections data to:
 * - Pre-process markdown content referenced in summary section lines
 * - Parse sidebar string that can be used in the VuePress config.js template
 * 
 * @param summarySections summary sections data
 * @param config Md Book Configuration Values
 * 
 * @returns sidebar string to parse into VuePress config.js template
 */
function preProcessParseSidebar(summarySections: SummarySection[], config: MdBookConfig) {
    // parse sidebar config
    let sidebar = '';

    for (const section of summarySections) {
        let sectionChildren = '';

        for (const line of section.summaryLines) {
            // read Markdown Content from input directory
            const mdFileLink = getMdContentLinkRef(line);
            const mdContentFilePath = join(config.mdContentPath, mdFileLink);
            let mdContentStr = readFileSync(mdContentFilePath, 'utf-8');

            // write Markdown Content to output directory (including pre processing Kroki.io Apis call's)
            writePreProcessedDestFile(config.mdContentPath, config.docsPath, mdContentFilePath, 
                preProcessKrokiMdContent(mdContentStr));

            // prepare section-children for parsing into sidebar-config
            let childLink = '/' + mdFileLink.split('.')[0];
            const rootLink = ROOT_LINKS.find(link => childLink.endsWith(link));

            if (rootLink) {
                // a rootlink must point to the directory (remove rootlink name)
                childLink = childLink.substr(0, childLink.length - rootLink.length)
            }
            
            const childName = getMdContentLinkName(line);
            sectionChildren += `                    ['${childLink}', '${childName}'],
`;
        }

        sidebar += `
               {
                  title: '${section.title}',
                  collapsable: false,
                  sidebarDepth: 0,
                  children: [
${sectionChildren}                  ]
               },`;
        
    }

    return sidebar;
}

/**
 * Export site markdown files as one Markdown-file based on summary sections
 * @param summarySections summary sections with lines that contains references to the site markdown files
 * @param config Markdown Book Configuration Values
 */
function exportSiteMd(summarySections: SummarySection[], config: MdBookConfig) {
    // initialize markdown vars
    let siteMdTitle = '';
    let siteMdContent = '';
    
    // Build markdown file from docs content
    for (const section of summarySections) {
        siteMdContent += '## ' + section.title + '\n\n';

        for (const line of section.summaryLines) {
            // add section content
            const mdFileLink = getMdContentLinkRef(line);
            const mdDocsFilePath = join(config.docsPath, mdFileLink);
            const mdLines = readFileSync(mdDocsFilePath, 'utf-8').split('\n');
            let mdContent = '';
            
            // increment markdown header levels
            for (const mdLine of mdLines) {

                if (mdLine.startsWith('#')) {
                    // Header Line

                    if (siteMdTitle == '') {
                        // first H1 is site title
                        // create YAML metadata for PANDOC https://pandoc.org/MANUAL.html#epub-metadata
                        siteMdTitle = [
                            '---',
                            `title: ${mdLine.split(' ')[1]}`,
                            `lang: ${config.lang}`,
                            '---',
                            '\n'
                        ].join('\n');
                    } else {
                        // Add Header as content line
                        mdContent += mdLine + '\n';
                    }

                } else {
                    // Add Content line
                    mdContent += mdLine + '\n';
                }

            }

            siteMdContent += mdContent;
        }

    }

    writeFileSync(config.siteMdFilePath, `${siteMdTitle}${siteMdContent}`);
}

/**
 * Install VuePress Template Dependencies
 * @param docsPath path to parsed VuePress docs build (required)
 */
export function InstallVuePressTemplateDependencies(config: MdBookConfig) {
    // install VuePress dependencies (VuePress, Theme, MarkedIt, Plugins, etc..)
    const dependencies: string[] = JSON.parse(
        readFileSync(config.vuePressDependenciesJsonFilePath, 'utf-8')
    );
    const dependenciesInstallResult = execSync(`npm install ${dependencies.join(' ')} --no-save`).toString();
    console.log(`dependenciesInstallResult = ${dependenciesInstallResult}`);
}