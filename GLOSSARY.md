---
terms:
    cli: "command line interpreter waarmee direct vanaf de prompt een commando of een script kan worden gestart"
    repository: "een centrale georganiseerd plaats voor opslag van content"
    git: "open source :repository: standaard"
    gitlab: "een op :git: gebaseerde implementatie"
---

# Glossary

<Glossary :terms="$frontmatter.terms" />
