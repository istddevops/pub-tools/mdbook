#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Node Package Modules
 */
const process_1 = require("process");
/**
 * Local Library Modules
 */
const lib_1 = require("../lib");
//
// START CLI Script
//
if (process_1.argv[2] || !process_1.argv[6]) {
    // create configuration from environment and CLI-parameters
    const gitLab = new lib_1.GitLabConfig(process_1.env);
    if (gitLab.ci) {
        console.log('='.repeat(40));
        console.log('CI variables available in GitLabConfig (TypeScript)');
        console.log('-'.repeat(40));
        console.log(`ciPagesDomain = ${gitLab.ciPagesDomain}`);
        console.log(`ciPagesUrl = ${gitLab.ciPagesUrl}`);
        console.log(`ciProjectRootNameSpace = ${gitLab.ciProjectRootNameSpace}`);
        console.log(`ciProjectNameSpace = ${gitLab.ciProjectNameSpace}`);
        console.log(`ciProjectName = ${gitLab.ciProjectName}`);
        console.log(`ciProjectPath = ${gitLab.ciProjectPath}`);
        console.log(`ciProjectTitle = ${gitLab.ciProjectTitle}`);
        console.log(`ciProjectUrl = ${gitLab.ciProjectUrl}`);
        console.log('='.repeat(40));
    }
    const mdContentPath = process_1.argv[3] ? process_1.argv[3] : '.';
    const docsPath = process_1.argv[4] ? process_1.argv[4] : 'docs';
    const templatePath = process_1.argv[5];
    const config = new lib_1.MdBookConfig(gitLab, { mdContentPath, docsPath, templatePath });
    // execute CLI-command
    const cliCommand = process_1.argv[2];
    switch (cliCommand) {
        case 'parse': {
            lib_1.parseMd2Book(config);
            break;
        }
        case 'vuepress': {
            lib_1.InstallVuePressTemplateDependencies(config);
            break;
        }
        default: {
            console.log('First argument (CLI-command) must be <parse | vuepress >!');
        }
    }
}
else {
    console.error('First two parms are required: <parse | build | dev> <mdContentPath> <docsPath> (<vuePressTemplatePath> is optional)');
}
