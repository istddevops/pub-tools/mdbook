import { GitLabConfig } from './gitlab-config';
/**
 * Mardown Book Publication Configuration
 */
export declare class MdBookConfig {
    private gitLab;
    private cliParms;
    private readonly vuePressConfig;
    private readonly vuePressPublic;
    private readonly vuePressStyles;
    private readonly vuePressConfigTemplateJs;
    private readonly vuePressDependenciesJson;
    private readonly vuePressConfigJs;
    private readonly siteMdFile;
    private readonly customLogoFile;
    private publication;
    /**
     * Site Export Language Metadata
     */
    readonly lang = "nl-NL";
    /**
     * Constructs Markdown Book Configuration Class
     * @param gitLab GitLab CI Running environment (in case of GitLab CI runner execution)
     * @param cliParms Command Line parameters
     */
    constructor(gitLab: GitLabConfig, cliParms: {
        mdContentPath: string;
        docsPath: string;
        templatePath?: string;
    });
    /**
     * Initialize Running GitLab CI Configuration values
     */
    private initGitLabCi;
    /**
     * Set custom publication logo ("logo.png" in repo Markdown content directory)
     */
    private setCustomLogo;
    /**
     * Resolved Output directory path to parse VuePress formated data
     */
    get docsPath(): string;
    /**
     * Resolved VuePress Public output directory path (output-files directly accessible in site publication)
     */
    get docsPublicPath(): string;
    /**
     * Resolved VuePress Styles output directory path
     */
    get docsStylesPath(): string;
    /**
     * Resolved Input directory path that should contain Md Book formated Markdown data
     */
    get mdContentPath(): string;
    /**
     * Resolved Path to VuePress Config Output JS File
     */
    get docsConfigJsFilePath(): string;
    /**
     * Resolved VuePress Template directory path (MdBook-package or customomized-repo)
     */
    get templatePath(): string;
    /**
     * Resolved Public template directory path (template-files directly accessible in site publication)
     */
    get templatePublicPath(): string;
    /**
     * Resolved Path to VuePress Styles template directory
     */
    get templateStylesPath(): string;
    /**
     * Resolved Path to VuePress Config Template JS File
     */
    get templateConfigJsFilePath(): string;
    /**
     * Resolved Path to VuePress Depedencies JSON File
     */
    get vuePressDependenciesJsonFilePath(): string;
    /**
     * Resolved Path to VuePress Site Export Input File (to create PDF, ePub, ...etc. exports)
     */
    get siteMdFilePath(): string;
    /**
     * GitLab Pages Publication base url path
     */
    get publicationBase(): string;
    /**
     * GitLab Pages Publication destination directory path
     */
    get publicationDest(): string;
    /**
     * GitLab Pages Publication logo path
     */
    get publicationLogo(): string;
    /**
     * GitLab Pages Publication PDF export URL
     */
    get publicationPdfExport(): string;
    /**
     * GitLab Pages Publication ePub export URL
     */
    get publicationEpubExport(): string;
    /**
     * GitLab Pages Publication GIT repository URL
     */
    get publicationGitRepo(): string;
}
