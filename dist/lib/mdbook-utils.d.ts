import { MdBookConfig } from './mdbook-config';
/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param config Markdown Book Configuration values
 */
export declare function parseMd2Book(config: MdBookConfig): void;
/**
 * Install VuePress Template Dependencies
 * @param docsPath path to parsed VuePress docs build (required)
 */
export declare function InstallVuePressTemplateDependencies(config: MdBookConfig): void;
