# Inleiding

Deze publicatie beschrijft de opzet en gebruik van **MdBook CLI** als voorlopige uitkomst voor een mogelijke boek-publicatie.

## Verkenning Publicatiewijze Boekvorm :green_book:

Deze verkenning is een eerste aanzet om te onderzoeken hoe een publicatie op basis van __Markdown__ input kan worden gerealiseerd. Er zijn diverse raamwerken waarmee dit kan worden gerealiseerd. Voor deze verkenning is gekozen voor een opzet als een zogenaamde boek publicatie (naar voorbeeld van de online service GitBook :blue_book:).
  
Belangrijkste kenmerlen van een boek publicatie vorm zijn:

- Het is een **SPA** (Single Page Application) en biedt daarmee responsiviteit om de publicatie te raadplegen op meerdere devices (PC, iPad, Smartphone's, etc.)

- De publicatie is op basis van **Statische HTLM** waarbij in veel gevallen **Markdown** als bron voor de content wordt gebruikt

- Er is ondersteuning om de publicatie in **PDF** en/of **ePub** formaat te exporteren waarbij ook gebruik op **eReaders** kan worden ondersteund

*Kortom de eisen die aan een de meeste huidige publicaties van standaarden, handliedingen en overige specificatie en dienst/product ondersteunende kunnen worden gevraagd.*

## Evaluatie beschikbare Publicatie-raamwerken

Er zijn meerdere raamwerken voor publicatie in een bookvorm uitgeprobeerd. Bronnen waren aanvankelijk door GitLab meegeleverde templates. Deze bleken echter niet geschikt. Daarom is vervolgens gekeken naar raamwerken die onder het
[![Ecosysteem: NPM](https://img.shields.io/badge/NPM-Ecosystem-brightgreen)](https://www.npmjs.com/)
volgens een [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](LICENSE) (of vergelijkbare licentie) beschikbaar worden gesteld. Momenteel lijkt [VuePress](https://github.com/vuejs/vuepress) de meest geschikte kandidaat voor de publicatie van functionele documentatie over standaarden. Deze voorbeeld-publicatie laat zien dat er standaard al veel zaken binnen het raamwerk worden voorzien zoals bijvoorbeeld de responsiviteit (daarmee leesbaar op verschillende devices).
  
Zie overzicht geevalueerde Publicatie-raamwerken:


| Publicatie-raamwerk | Korte Eveluatie | Score |
|-|:-|:-|
| [GitBook](https://github.com/GitbookIO/gitbook/blob/master/README.md) | Is beschikbaar als template op GitLab maar Open Source versie wordt niet meer ondersteund (depreciated) en is alleen nog maar beschikbaar als OnLine Service en is dus niet meer op lange termijn als raamwerk te gebruiken. | :-1: :-1: |
| [Jekyll](https://github.com/jekyll/jekyll) | Heeft standaard GitHub Pages support en werkt ook via een standaard template voor GitLab Pages. Veel Contributers en support alleen groot nadeel is de afhanelijkheid van Ruby Gems Platform waarmee het niet eenvoudig is om lokaal publicaties te ontwikkelen. Ook bleek het (hergebruiken) van een Book Theme bleek via GitLab Pages niet te werken. | :-1: :-1: |
| [Gatsby](https://github.com/gatsbyjs/gatsby) | Leek in eerste instantie standaard NPM maar gaf wat problemen met lokale installatie. Ook gevonden via de GitLab template maar ook dat werkte niet helemaal. Vooral een theme vinden en werkend krijgen om uit te proberen was wat moeilijk. Samengevat te veel twijfels over omdat ik niet goed kon zien hoeveel Contributers er nu waren. | :-1: |
| [VuePress](https://github.com/vuejs/vuepress) | Werkt eenvoudig zowel lokaal ontwikkelen van Books als CI / DD opleveren op GitLab Pages. Ook is er op GitBook geinspireerd Theme [VuePress Theme Book](https://github.com/cyrilf/vuepress-theme-book) dat zeer goed werkt. Nog bezig met verkennen maar nu al veelbelovend (en veel potentiele handige plugin's beschikbaar). Ook een grote grote groep Contributers maakt het geschikt voor een lange termijn Open Source oplossing | :+1:

## Gebruik Emoticons

Informatie over gebruik Emoticons:

- Zie voor een [uitputtende lijst met Emoticios op GitHub](https://gist.github.com/rxaviers/7360908)

